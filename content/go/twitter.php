<!doctype html>
<html>
<head>
<meta charset="utf-8">
<?php
header("location: https://twitter.com/MineLume?s=20");
exit;
?>
<title>MineLume - Redirecting to Tiwtter</title>
<!--Favicon-->
<link rel="icon" href="../img/servericon.png" type="image/png">
<!--meta-->
<meta name="viewport" content="width=device-width, initial-scale=1">
<!--internal-stylesheet-->	
<link rel="stylesheet" href="../style/main.css">
<!--external-stylesheet-->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
<!--external-scripts-->
<script src="https://kit.fontawesome.com/a076d05399.js"></script>
<!--internal-scripts-->
<script>
jQuery(document).ready(function( $ ){
  // Get page title
  	var pageTitle = $("title").text();
  
  // Change page title on blur
  $(window).blur(function() {
	  $("title").text("MineLume - Network out of passion");
	});

	// Change page title back on focus
	$(window).focus(function() {
	  $("title").text(pageTitle);
	});
});	
</script>	
</head>
<body>
	<!--Banner-->
	<div class="container col-12 banner">
		<img src="../img/MineLumeLogoWithText.png" alt="MineLumeLogoWithText">
	</div>
	<!--Navigation-->
	<div class="container col-12" style="padding: 0px;">
		<nav class="navbar navbar-expand-sm sticky-top nav-global">
			<a class="navbar-brand"> </a>
			<!-- Toggler/collapsibe Button -->
			<button class="navbar-toggler button-color" type="button" data-toggle="collapse" data-target="#collapsibleNavbar">
				<i class="fas fa-bars" style="font-size: 25px; float: left;"></i>
			</button>

			<!-- Navbar links -->
			<div class="collapse navbar-collapse justify-content-center" id="collapsibleNavbar">
				<ul class="navbar-nav">
					<li class="nav-item">
						<a class="nav-link" href="#">HOME</a>
					</li>
					<li class="nav-item">
						<a class="nav-link" href="#">FORUM</a>
					</li>
					<li class="nav-item">
						<a class="nav-link" href="#">SHOP</a>
					</li>
					<li class="nav-item">
						<a class="nav-link" href="#">SUPPORT</a>
					</li>
				</ul>
			</div>	
		</nav>
	</div>
	<!--Slider-->
	<div class="container col-12" style="padding: 0px;">
	<div id="slide" class="carousel slide" data-ride="carousel">
		<ul class="carousel-indicators">
			<li data-target="#slide" data-slide-to="0" class="active"></li>
			<li data-target="#slide" data-slide-to="1"></li>
			<li data-target="#slide" data-slide-to="2"></li>
		</ul>
		<div class="carousel-inner">
		<div class="carousel-item active ">
		<center><img src="../img/slide-bowbash.jpg" alt="BowBashMap Fantasy"></center>
		<div class="carousel-caption" style="color: black; top: 20px; text-align: center;">
		<p class="upper_caption">BowBash kommt zurück!</p>
		<p class="lower_caption">Ein alt bekannter Spielmodus kehrt heim!</p>
		</div>		
		</div>
		<div class="carousel-item">
		<center><img src="../img/slide-guessit.jpg" alt="GuessIt wird fantastisch"></center>
		<div class="carousel-caption" style="color: black; top: 20px; text-align: center;">
		<p class="upper_caption">GuessIt kommt nach Hause!</p>	
		<p class="lower_caption">Ein Zuhause mit Kreativität gefunden!</p>
		</div>
		</div>
		<div class="carousel-item">
		<center><img src="../img/slide-minewars.jpg" alt="GuessIt wird fantastisch"></center>
		<div class="carousel-caption" style="color: black; top: 20px; text-align: center;">
		<p class="upper_caption">MineWars - NEUHEIT!</p>	
		<p class="lower_caption">Die Kreativität lebe hoch!</p>
		</div>
		</div>
		</div>
		<a class="carousel-control-prev" href="#slide" data-slide="prev">
			<span class="carousel-control-prev-icon"></span>
		</a>
		<a class="carousel-control-next" href="#slide" data-slide="next">
			<span class="carousel-control-next-icon"></span>
		</a>
		</div>
	</div>
</body>
</html>